# React Native

## Installation

1. Install node (at least 4).

    If you found the old node package installed, run this command to completely remove it:

        $ sudo apt-get remove --purge node

    To install Node.js, open a terminal and type the following command:

        $ sudo apt-get install nodejs

    Then install the node package manager, npm:

        $ sudo apt-get install npm

    Then, check if Java is not already installed:

        $ java -version

    @ref: http://www.hostingadvice.com/how-to/install-nodejs-ubuntu-14-04/

2. Install Java:

    $ sudo apt-get install default-jre
    $ sudo apt-get install default-jdk

    @ref: https://www.digitalocean.com/community/tutorials/how-to-install-java-on-ubuntu-with-apt-get

    Install Java8:

    $ sudo add-apt-repository ppa:webupd8team/java
    $ sudo apt-get update
    $ sudo apt-get install oracle-java8-installer
    $ sudo apt-get install oracle-java8-set-default

    @ref: http://tecadmin.net/install-oracle-java-8-jdk-8-ubuntu-via-ppa/

3. Install Android Studio.

    Follow the steps from android site: https://developer.android.com/sdk/installing/index.html:

    $ sudo apt-get install zlib.i686 ncurses-libs.i686 bzip2-libs.i686
    $ cd /home/lau/Downloads/
    $ cd android-studio
    $ cd bin
    $ sudo chmod 777 -R studio.sh
    $ ./studio.sh

    @ref: https://www.youtube.com/watch?v=axtVId9ASmY

    After the installation, make sure the ANDROID_HOME environment variable points to your existing Android SDK:
    export ANDROID_HOME=<path_where_you_unpacked_android_sdk>

    $ vim ~/.bashrc
    $ export ANDROID_HOME=~/Android/Sdk
    $ export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

    Then:

    $ vim ~/.bash_profile
    $ export ANDROID_HOME=~/Android/Sdk
    $ export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

4. Install React Native command line tools:

    $ sudo npm install -g react-native-cli

    @ref: http://facebook.github.io/react-native/docs/getting-started.html

5. Create Create a React Native project:

    $ react-native init AwesomeProject

    @ref: http://facebook.github.io/react-native/docs/getting-started.html

6. Install an emulator - Genymotion:

    $ chmod +x <Genymotion installer path>/genymotion-<version>_<arch>.bin
    $ cd <Genymotion installer path>
    $ ./genymotion-<version>_<arch>.bin -d <Genymotion installer path>
    $ cd <Genymotion installer path>
    $ ./genymotion

    For an example:

    $ chmod +x /home/lau/Downloads/genymotion-2.6.0-linux_x64.bin
    $ cd /home/lau/Downloads/
    $ ./genymotion-2.6.0-linux_x64.bin -d /home/lau/Downloads/
    $ cd /home/lau/Downloads/genymotion/
    $ ./genymotion

    Change the settings and then select an android device. Start the device.

7. Connect to a mobile device.

    To list the connected devices, type:

    $ lsusb

8. Run the React Native project:

    $ npm start

    Then open a new tab, type:

    $ react-native run-android

## Working on the code

1. Fire up the editor or IDE you decided to use and point it at the HelloWorld folder you created. In it, you will find a file named index.android.js which you need to open in your editor.

    Change line 19 from

    `Welcome to React Native!`

    to

    `Hello, World!`

2. Then switch back to the Android Simulator (Genymotion) window and hit F1 and click on "Reload JS". The UI of our app will refresh and display the new text.

@ref: http://beginning-mobile-app-development-with-react-native.com/book-preview.html

### Chrome debugger

    http://localhost:8081/debugger-ui

### There is no DOM

    Not surprisingly, there is no DOM on mobile. Where we previously used “<div />” we need to use “<View />” and where we used “<span />” the component we need here is “<Text />”.

    So, from:

    <div className="box" onClick={this.clickMe.bind(this)}>Hello {this.props.name}. Please click me.</div>

    Change it to:

    <View className="box" onClick={this.clickMe.bind(this)}>Hello {this.props.name}. Please click me.</View>

    While it’s quite convenient to put text directly in “<div />” elements, in the native world text can’t be put directly in a “<View />”. For that we need to insert a “<Text />” component.

    So, change it to:

    <View className="box" onClick={this.clickMe.bind(this)}>
        <Text>Hello {this.props.name}. Please click me.</Text>
    </View>

### Handling Events

    The equivalent to clicking in web pages is tapping an element on the mobile device. Let’s change our code so that the “alert” pops up when we tap the element.

    Instead of events being directly available on “<View />” components, we need to explicitly use elements that trigger events, in our case a touch event when pressing the view. There are different types of touchable components available, each of them providing a different visual feedback.

    <TouchableOpacity onPress={this.clickMe}>
        <View style={styles.box}>
            <Text>Hello {this.props.name}. Please click me.</Text>
        </View>
    </TouchableOpacity>

### Registering the Application

    When developing with React for the browser, we just need to define a mount point, call "ReactDOM.render", and let React do its magic. In React Native, this is a little bit different.

    AppRegistry.registerComponent('AwesomeProject', function() {
        return AwesomeProject;
    });

    We have to register the component for the Java/ Objective-C side of things which is done using the “AppRegistry” object. The name we give has to match with the name inside the Android/ Xcode project.

    Our Hello World React Native application has significantly more lines of code than its web counterpart, but on the other side React Native takes separation of concern a bit further especially because styles are defined with the component.

    The entire code:

    import React, {
      AppRegistry,
      Component,
      StyleSheet,
      Text,
      View,
      TouchableOpacity
    } from 'react-native';

    class AwesomeProject extends Component {
      clickMe() {
        alert('Hi!');
      }
      render() {
        return (
          <View style={styles.container}>
            <TouchableOpacity onPress={this.clickMe}>
              <View>
                <Text>Hello {this.props.name}. Please click me.</Text>
              </View>
            </TouchableOpacity>
          </View>
        );
      }
    }

    const styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
      }
    });

    class MainComponent extends Component {
      render() {
        return (
          <AwesomeProject name="Component" />
        );
      }
    }

    AppRegistry.registerComponent('AwesomeProject', function() {
        return MainComponent;
    });

    Or:

    With ES6 arrow functions:

    AppRegistry.registerComponent('AwesomeProject', () => MainComponent);

    @ref: https://www.toptal.com/ios/cold-dive-into-react-native-a-beginners-tutorial
    @ref: http://blog.mojotech.com/from-react-to-react-native-in-5-minutes/

### Navigator

    Sample code:

    Import components/ dependencies:

    import React, {
      AppRegistry,
      Component,
      StyleSheet,
      Text,
      View,
      Navigator,
      TouchableOpacity
    } from 'react-native';

    Create pages:

    class PageOne extends Component {
       _handlePress() {
        this.props.navigator.push({id: 2,});
      }
      render() {
        return (
          <View style={[styles.container, {backgroundColor: 'green'}]}>
            <Text style={styles.welcome}>Greetings!</Text>
            <TouchableOpacity onPress={this._handlePress.bind(this)}>
              <View style={{paddingVertical: 10, paddingHorizontal: 20, backgroundColor: 'black'}}>
                <Text style={styles.welcome}>Go to page two</Text>
              </View>
            </TouchableOpacity>
           </View>
        );
      }
    }

    class PageTwo extends Component {
       _handlePress() {
        this.props.navigator.pop();
      }
      render() {
        return (
          <View style={[styles.container, {backgroundColor: 'purple'}]}>
            <Text style={styles.welcome}>This is page two!</Text>
            <TouchableOpacity onPress={this._handlePress.bind(this)}>
              <View style={{paddingVertical: 10, paddingHorizontal: 20, backgroundColor: 'black'}}>
                <Text style={styles.welcome}>Go back</Text>
              </View>
            </TouchableOpacity>
           </View>
        );
      }
    }

    Add Navigator to the main component:

    class AwesomeProject extends Component {

      _renderScene(route, navigator) {
        if (route.id === 1) {
          return <PageOne navigator={navigator} />
        } else if (route.id === 2) {
          return <PageTwo navigator={navigator} />
        }
      }

      render() {
        return (
          <Navigator
            initialRoute={{id: 1, }}
            renderScene={this._renderScene}
            configureScene={
                function (route) {
                  if (route.sceneConfig) {
                    return route.sceneConfig;
                  }
                  return Navigator.SceneConfigs.FloatFromRight;
                }
            }
           />
        );
      }
    }

    const styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
      },
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: 'white',
      },
    });

    AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);

    @ref: https://github.com/h87kg/NavigatorDemo/blob/master/index.android.js
    @ref: http://blog.paracode.com/2016/01/05/routing-and-navigation-in-react-native/
    @ref: https://rnplay.org/plays/L9v2wg
    @ref: https://rnplay.org/apps/HPy6UA

### Other code/ app samples

    * https://facebook.github.io/react-native/docs/tutorial.html
    * http://herman.asia/building-a-flashcard-app-with-react-native
    * http://www.appcoda.com/react-native-introduction/
    * http://code.tutsplus.com/tutorials/creating-a-dictionary-app-using-react-native-for-android--cms-24969
    * https://www.raywenderlich.com/126063/react-native-tutorial
    * https://medium.com/@dabit3/react-native-navigator-navigating-like-a-pro-in-react-native-3cb1b6dc1e30#.nx42jeu89
    * https://github.com/facebook/react-native/tree/master/Examples/Movies

## Troubleshooting

1. 'failed to find Build Tools revision 23.0.1'

    **Solution**

    Show SDK list:

    $ android list sdk -a

    Then:

    $ android update sdk -a -u -t 5

    Or, where the a parameter installs all packages. Not the best solution to install everything, but it has at least installed the required build tools version:

    $ android update sdk -u -a

    @ref: http://stackoverflow.com/questions/27272605/failed-to-find-build-tools-revision-21-1-1-sdk-up-to-date
    @ref: http://satomi.hatenablog.jp/entry/2015/12/23/203304

2. 'android: command not found'

    Fix is in the installation steps #3 - Install Android Studio.

    @ref: https://forum.ionicframework.com/t/android-home-is-not-set-and-android-command-not-in-your-path/14821/25

3. 'error: cannot connect to daemon'

    Go to Genymotion's Settings -> ABD

    Select 'Use custom Android SDK tools', then add the path into the input field:

    /home/lau/Android/Sdk

    @ref: http://stackoverflow.com/questions/35959350/react-native-android-genymotion-adb-server-didnt-ack

4. 'Could not connect to development server'

    You'll see a red screen with an error with this URL below:

    http://localhost:8081/index.android.bundle?platform=android&dev=true&hot=false&minify=false

    This is OK. You just need to change localhost to your machine's IP address. The following steps will fix that.

    **Solution**

    1. Open the Developer menu by shaking the device or running adb shell input keyevent 82 from the command line.
    2. Go to Dev Settings.
    3. Go to Debug server host for device.
    4. Type in your machine's IP address and the port of the local dev server (e.g. 10.124.13.7:8081) -type `ifconfig` to find your machine's IP address.
    5. Go back to the Developer menu and select Reload JS.

    @ref: https://facebook.github.io/react-native/docs/running-on-device-android.html#content
    @ref: https://blog.hainuo.info/blog/react-native-152.html

5. Devtools "React" Tab Does Not Work

    It's currently not possible to use the "React" tab in the devtools to inspect app widgets. This is due to a change in how the application scripts are evaluated in the devtools plugin; they are now run inside a Web Worker, and the plugin is unaware of this and so unable to communicate properly with React Native.

    @ref: https://www.youtube.com/watch?v=1Q35qUzYLWs
    @ref: https://facebook.github.io/react-native/docs/known-issues.html#content
    @ref: https://github.com/facebook/react-native/issues/3373

    There might be a workaround here:

    @ref: http://odino.org/enable-remote-debugging-on-chrome-for-react-natives-webviews/

6. Cannot launch AVD in emulator. Output: sh: 1: glxinfo:

    $ sudo apt-get update && sudo apt-get install mesa-utils

    Or:

    $ sudo apt-get install lib64stdc++6
    $ cd $ANDROID_HOME/tools/lib64/libstdc++
    $ mv libstdc++.so.6 libstdc++.so.6.bak
    $ ln -s /usr/lib64/libstdc++.so.6 $ANDROID_HOME/tools/lib64/libstdc++

    @ref: http://stackoverflow.com/questions/36554322/cannot-start-emulator-in-android-studio-2-0
    @ref: http://stackoverflow.com/questions/36258908/cannot-launch-avd-in-emulator-output-sh-1-glxinfo

## Launch Android Studio

On your terminal, type:

    $ cd /home/lau/Downloads/android-studio/bin
    $ ./studio.sh

    @ref: https://www.youtube.com/watch?v=3tb7AiRyNUM

## Install an APK into your emulator

1. Download an APK from https://apkpure.com/app

2. Make sure the emulator is open, such as Genymotion. Or use Android Studio's by typing the command below in your terminal:

    To start an instance of the emulator from the command line, navigate to the tools/ folder of the SDK. Enter emulator command like this:

    $ emulator -avd <avd_name> [<options>]

    For an example:

    $ cd /home/lau/Android/Sdk/tools/
    $ emulator -avd Nexus_6_API_23

    Note that this emulator must be created in Android Studio AVD manager first.

    @ref: http://developer.android.com/tools/help/emulator.html

3. Install the APK:

    $ /home/lau/Android/Sdk/platform-tools/adb install /home/lau/Downloads/WhatsApp_Messenger_v2.16.67_apkpure.com.apk

## WebView

A web container that allows mobile devices to execute JavaScript and HTML5. WebView (called various terms for differing platforms) helps bridge the gap between web and mobile, and various plugins help make an application more robust and native-feeling.

@ref: https://www.checkmarx.com/2015/10/23/the-worst-phonegap-security-issues-and-how-to-avoid-them/

## Cross-site Scripting (XSS)

Cross-site Scripting (XSS) refers to client-side code injection attack wherein an attacker can execute malicious scripts (also commonly referred to as a malicious payload) into a legitimate website or web application.

@ref: https://en.wikipedia.org/wiki/Cross-site_scripting
@ref: http://www.acunetix.com/websitesecurity/cross-site-scripting/
@ref: https://www.owasp.org/index.php/Cross-site_Scripting_(XSS)

# React

## State

State is simply data that can change. This data is called state, because it represents the “state” of your application. If this data changes, your application will likely look different, hence being in a different “state”. State is usually things like a list of todos, or an enabled/disabled button.

The root component will serve as the holder of state. State changes start at the root component, which is then responsible for updating the properties of its child components.

Component properties are immutable, meaning they can’t be modified. So, if you can’t modify the properties, how do they ever change? You re-render, the application by calling setState(), like seen in componentDidMount().

The setState() function is special because every time it is called, it will attempt to re-render the entire app. This means that if a child property is different from the last state, it will be re-rendered with the new value.

@ref: https://www.firebase.com/blog/2016-01-20-tutorial-firebase-react-native.html

# ES6

ECMAScript is a scripting language that is standardised by a company called Ecma International. JavaScript is an implementation of ECMAScript. ECMAScript 6 is simply the next version of the ECMAScript standard and, hence, the next version of JavaScript. The spec aims to be fully comfirmed and complete by the end of 2014, with a target initial release date of June 2015. It’s impossible to know when we will have full feature support across the most popular browsers, but already some ES6 features are landing in the latest builds of Chrome and Firefox. You shouldn’t expect to be able to use the new features across browsers without some form of additional tooling or library for a while yet.

@ref: https://developer.mozilla.org/en/docs/web/javascript/reference/statements/import
@ref: https://24ways.org/2014/javascript-modules-the-es6-way/

## module system: require vs ES6 import

1. Importing modules using require, and exporting using module.exports and exports.foo
2. Importing modules using ES6 import, and exporting using ES6 export

## import

The import statement is used to import functions, objects or primitives that have been exported from an external module, another script, etc.

## class and inheritance

@ref: http://javascriptplayground.com/blog/2014/07/introduction-to-es6-classes-tutorial/
@ref: http://arlimus.github.io/articles/ready.for.es6/
@ref: http://weblogs.asp.net/dwahlin/getting-started-with-es6-%E2%80%93-the-next-version-of-javascript
@ref: http://www.2ality.com/2015/02/es6-classes-final.html
@ref: https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Classes
@ref: http://www.sitepoint.com/the-es6-conundrum/
