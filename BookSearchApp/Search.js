/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, {
  StyleSheet,
  View,
  Text,
  Component
} from 'react-native';

class Search extends Component {

  render() {
    return (
      <View style={styles.container}>
          <Text style={styles.description}>
            Search Tab
          </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  description: {
    fontSize: 20,
    backgroundColor: 'white'
  },
});

export Search;
