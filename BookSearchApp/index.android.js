/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    View,
    Navigator
} from 'react-native';

import BookList from './BookList';
import BookDetail from './BookDetail';

class AwesomeProject extends Component {

  _renderScene(route, navigator) {
    if (route.passProps) {
      // …route.passProps — when pushing to a route, allows a passProps property to pass props to next to component
      return <BookDetail navigator={navigator} {...route.passProps} />
    } else {
      return <BookList navigator={navigator} />
    }
  }

  render() {
    return (
      <Navigator
        initialRoute = {{
            title: 'Featured Books',
            component: BookList
        }}
        renderScene = {this._renderScene}
        configureScene = {
            function (route) {
              if (route.sceneConfig) {
                return route.sceneConfig;
              }
              return Navigator.SceneConfigs.FloatFromRight;
            }
        }
       />
    );
  }
}

AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);
