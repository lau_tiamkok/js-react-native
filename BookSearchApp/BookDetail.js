/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, {
    StyleSheet,
    View,
    Text,
    Image,
    Component,
    ScrollView,
    TouchableOpacity
} from 'react-native';

var FAKE_BOOK_DATA = [
    {volumeInfo: {title: 'The Catcher in the Rye', authors: "J. D. Salinger", imageLinks: {thumbnail: 'http://books.google.com/books/content?id=PCDengEACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api'}, 'description': 'bla bla bla'}}
];

class BookDetail extends Component {

    _handlePress() {
        this.props.navigator.pop();
    }

    render() {
        var book = this.props.book;
        // var book = FAKE_BOOK_DATA[0];
        var imageURI = (typeof book.volumeInfo.imageLinks !== 'undefined') ? book.volumeInfo.imageLinks.thumbnail : '';
        var description = (typeof book.volumeInfo.description !== 'undefined') ? book.volumeInfo.description : '';
        return (
            <View>
                <ScrollView
                    ref={(scrollView) => { _scrollView = scrollView; }}
                    automaticallyAdjustContentInsets={false}
                    onScroll={() => { console.log('onScroll!'); }}
                    scrollEventThrottle={200}
                    style={styles.scrollView}>
                        <View style={styles.container}>
                            <View style={{padding: 10}}><Image style={styles.image} source={{uri: imageURI}} /></View>
                            <View style={{padding: 10}}><Text style={styles.description}>{description}</Text></View>
                        </View>
                        <TouchableOpacity
                              style={styles.button}
                              onPress={() => { _scrollView.scrollTo({y: 0}); }}>
                              <Text>Scroll to top</Text>
                        </TouchableOpacity>
                </ScrollView>
                <TouchableOpacity
                      style={styles.button}
                      onPress={this._handlePress.bind(this)}>
                      <Text>Back</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 75,
        alignItems: 'center'
    },
    image: {
        width: 107,
        height: 165
    },
    description: {
        fontSize: 15,
        color: '#656565'
    },
    scrollView: {
        backgroundColor: '#6A85B1',
        height: 400,
    },
    button: {
        margin: 7,
        padding: 5,
        alignItems: 'center',
        backgroundColor: '#eaeaea',
        borderRadius: 3,
    },
});

export default BookDetail;
